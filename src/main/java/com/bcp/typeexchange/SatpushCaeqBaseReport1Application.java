package com.bcp.typeexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SatpushCaeqBaseReport1Application {

	public static void main(String[] args) {
		SpringApplication.run(SatpushCaeqBaseReport1Application.class, args);
	}

}
