package com.bcp.typeexchange.commons;

public class Constant {

    public static final String MESSAGE_WELCOME = "BIENVENIDO ";
    public static final String NOT_EXISTS_USER = "El Usuario no existe vuelva a intentar";

    private Constant() {
        throw new IllegalStateException("Utility class");
    }
}
