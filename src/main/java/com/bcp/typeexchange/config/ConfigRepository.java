package com.bcp.typeexchange.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;

import io.r2dbc.spi.ConnectionFactory;

@SpringBootApplication
public class ConfigRepository {
    @Bean
    ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {

        ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
        initializer.setConnectionFactory(connectionFactory);
        initializer.setDatabasePopulator(new ResourceDatabasePopulator(new ByteArrayResource(("DROP TABLE IF EXISTS user;"
                + "CREATE TABLE user (id BIGINT PRIMARY KEY auto_increment, username VARCHAR(120) NOT NULL, password VARCHAR(120) NOT NULL, role VARCHAR(50) NOT NULL);"
                + "DROP TABLE IF EXISTS exchange_rate;"
                + "CREATE TABLE exchange_rate (currency VARCHAR(10) PRIMARY KEY, value DECIMAL(20,4) NOT NULL);"
                + "INSERT INTO user (username, password, role) VALUES ('cmotta', 'cmotta123', 'admin');"
                + "INSERT INTO user (username, password, role) VALUES ('user1', 'user1', 'engineer');"
                + "INSERT INTO exchange_rate (currency, value) VALUES ('USD', 0.27);"
                + "INSERT INTO exchange_rate (currency, value) VALUES ('PEN', 1);"
                + "INSERT INTO exchange_rate (currency, value) VALUES ('EUR', 0.24);").getBytes())));
        return initializer;

    }
}
