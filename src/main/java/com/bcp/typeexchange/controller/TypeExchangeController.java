package com.bcp.typeexchange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.typeexchange.model.ExchangeRate;
import com.bcp.typeexchange.model.ExchangeRequest;
import com.bcp.typeexchange.model.ExchangeResponse;
import com.bcp.typeexchange.model.UserRequest;
import com.bcp.typeexchange.model.UserResponse;
import com.bcp.typeexchange.service.IUserService;

import io.reactivex.Completable;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class TypeExchangeController {

    @Autowired
    private IUserService iUserService;

    @GetMapping("/log-in")
    public Mono<UserResponse> logIn(UserRequest userRequest) {
        UserResponse resptInfo = iUserService.findUser(userRequest).block();
        return Mono.just(resptInfo);
    }

    @GetMapping("/exchange-value")
    public Mono<ExchangeResponse> exchangeValue(ExchangeRequest exchangeRequest) {
        ExchangeResponse resptInfo = iUserService.exchangeValue(exchangeRequest).block();
        return Mono.just(resptInfo);
    }

    @PostMapping("/save-exchange")
    public Completable saveExchange(ExchangeRequest exchangeRequest) {
        return iUserService.saveExchange(exchangeRequest);
    }

    @GetMapping("/get-exchange")
    public Mono<ExchangeRate> getExchange(ExchangeRequest exchangeRequest) {
        return Mono.just(iUserService.getExchange(exchangeRequest).block());
    }

}
