package com.bcp.typeexchange.model;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;

@Data
@Table(value = "exchange_rate")
public class ExchangeRate {

    @Id
    private String     currency;
    private BigDecimal value;
}
