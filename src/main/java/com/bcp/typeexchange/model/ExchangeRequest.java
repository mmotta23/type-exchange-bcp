package com.bcp.typeexchange.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeRequest {

    String     originCurrency;
    BigDecimal value;
    String     destinyCurrency;
    String     userName;
    String     role;

}
