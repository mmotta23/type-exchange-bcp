package com.bcp.typeexchange.model;

import java.math.BigDecimal;

public class ExchangeResponse {
    private String     originCurrency;
    private BigDecimal originValue;
    private String     destinyCurrency;
    private BigDecimal destinyValue;
    private BigDecimal exchangeRate;

    public ExchangeResponse(String originCurrency, BigDecimal originValue, String destinyCurrency, BigDecimal destinyValue,
            BigDecimal exchangeRate) {
        this.originCurrency = originCurrency;
        this.originValue = originValue;
        this.destinyCurrency = destinyCurrency;
        this.destinyValue = destinyValue;
        this.exchangeRate = exchangeRate;
    }

    public String getOriginCurrency() {
        return originCurrency;
    }

    public void setOriginCurrency(String originCurrency) {
        this.originCurrency = originCurrency;
    }

    public BigDecimal getOriginValue() {
        return originValue;
    }

    public void setOriginValue(BigDecimal originValue) {
        this.originValue = originValue;
    }

    public String getDestinyCurrency() {
        return destinyCurrency;
    }

    public void setDestinyCurrency(String destinyCurrency) {
        this.destinyCurrency = destinyCurrency;
    }

    public BigDecimal getDestinyValue() {
        return destinyValue;
    }

    public void setDestinyValue(BigDecimal destinyValue) {
        this.destinyValue = destinyValue;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

}
