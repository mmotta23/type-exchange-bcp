package com.bcp.typeexchange.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;

@Data
@Table(value = "user")
public class User {
    @Id
    private long   id;
    private String username;
    private String password;
    private String role;

}
