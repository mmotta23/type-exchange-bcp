package com.bcp.typeexchange.repository;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.bcp.typeexchange.model.ExchangeRate;

import reactor.core.publisher.Mono;

@Repository
public interface ExchangeRateRepository extends ReactiveCrudRepository<ExchangeRate, String> {
    @Query("SELECT * FROM exchange_rate WHERE currency=:currency")
    Mono<ExchangeRate> findByCurrency(String currency);
}
