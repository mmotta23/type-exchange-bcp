package com.bcp.typeexchange.service;

import com.bcp.typeexchange.model.ExchangeRate;
import com.bcp.typeexchange.model.ExchangeRequest;
import com.bcp.typeexchange.model.ExchangeResponse;
import com.bcp.typeexchange.model.UserRequest;
import com.bcp.typeexchange.model.UserResponse;

import io.reactivex.Completable;
import reactor.core.publisher.Mono;

public interface IUserService {
    public Mono<UserResponse> findUser(UserRequest userRequest);

    public Mono<ExchangeResponse> exchangeValue(ExchangeRequest exchangeRequest);

    Completable saveExchange(ExchangeRequest exchangeRequest);

    Mono<ExchangeRate> getExchange(ExchangeRequest exchangeRequest);
}
