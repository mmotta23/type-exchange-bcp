package com.bcp.typeexchange.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.typeexchange.commons.Constant;
import com.bcp.typeexchange.model.ExchangeRate;
import com.bcp.typeexchange.model.ExchangeRequest;
import com.bcp.typeexchange.model.ExchangeResponse;
import com.bcp.typeexchange.model.User;
import com.bcp.typeexchange.model.UserRequest;
import com.bcp.typeexchange.model.UserResponse;
import com.bcp.typeexchange.repository.ExchangeRateRepository;
import com.bcp.typeexchange.repository.UserRepository;

import io.reactivex.Completable;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class UserService implements IUserService {
    @Autowired
    private UserRepository         userRepository;
    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

    @Override
    public Mono<UserResponse> findUser(UserRequest userRequest) {
        User userInfo = userRepository.findByUsername(userRequest.getUserName()).block();
        String messageLogIn = null;
        if (Objects.nonNull(userInfo)) {
            messageLogIn = Constant.MESSAGE_WELCOME.concat(userRequest.getUserName());
        } else {
            messageLogIn = Constant.NOT_EXISTS_USER;
        }
        return Mono.just(new UserResponse(userRequest.getUserName(), messageLogIn));
    }

    @Override
    public Mono<ExchangeResponse> exchangeValue(ExchangeRequest exchangeRequest) {
        ExchangeRate exchangeOrigin = exchangeRateRepository.findByCurrency(exchangeRequest.getOriginCurrency()).block();
        ExchangeRate exchangeDestiny = exchangeRateRepository.findByCurrency(exchangeRequest.getDestinyCurrency()).block();
        log.info("Method [exchangeValue] User: " + exchangeRequest.getUserName() + ", Role: " + exchangeRequest.getRole());
        return Mono.just(
                new ExchangeResponse(exchangeRequest.getOriginCurrency(), exchangeRequest.getValue(), exchangeRequest.getDestinyCurrency(),
                        exchangeRequest.getValue().multiply(exchangeOrigin.getValue()).divide(exchangeDestiny.getValue()),
                        exchangeOrigin.getValue().divide(exchangeDestiny.getValue())));
    }

    @Override
    public Completable saveExchange(ExchangeRequest exchangeRequest) {
        ExchangeRate dataInsert = new ExchangeRate();
        dataInsert.setCurrency(exchangeRequest.getOriginCurrency());
        dataInsert.setValue(exchangeRequest.getValue());
        exchangeRateRepository.save(dataInsert);
        log.info("Method [saveExchange] User: " + exchangeRequest.getUserName() + ", Role: " + exchangeRequest.getRole());
        return Completable.complete();
    }

    @Override
    public Mono<ExchangeRate> getExchange(ExchangeRequest exchangeRequest) {
        return Mono.just(exchangeRateRepository.findByCurrency(exchangeRequest.getOriginCurrency()).block());
    }
}
